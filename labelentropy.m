function [entropy, maxp] = labelentropy(im)

base = 10;
eps = 1e-05;

h = @(p) -sum(p.*log2(p+eps)/log2(base),2);
 
c=hist(im(:),[1:base]);
[maxv, maxp] = max(c);

entropy = 1-h(c/sum(c));

            