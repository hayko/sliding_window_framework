function [maxp] = labelmajority(im)

base = 10;
c=hist(im(:),[1:base]);
[~, maxp] = max(c);


            