function feats=compute_raw_features(R,W,H,locW,locH)

feats = cell(size(locW,1)*size(locW,2),1);

feat_indx = 1;
%loop over locations
for posi=1:size(locW,1)
    %display ([num2str([posi])])
    for posj=1:size(locW,2)
        posw = locW(posi,posj);
        posh = locH(posi,posj);
        
        % display ([num2str([posh posh+H-1 posw posw+W-1])])
        feats{feat_indx} = R(posh:posh+H-1,posw:posw+W-1,:);
        feat_indx = feat_indx+1;
    end
end
end
