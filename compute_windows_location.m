function [feats,win_posw,win_posh,winw,winh] = compute_windows_location(II, window_size, scales, strideh,stridew,method)
% compute_windows_location(II, window_size, scales, strideh,stridew)
% generates sliding window positions and returns features extract at the 
% window position and the bboxes of all positions, across multiple scales.
%
% Parameter
%
%  II           input image for feature extraction (crops are returned)
%  window_size  initial scale 1 search window [height width]
%  scales       scale range to be processed [0.5 1 2], 
%               e.g. ratio = 2^(1/4); num = 2; scales = ratio.^(-num:num);
%               scales = [0.7071    0.8409    1.0000    1.1892    1.4142];
%  strideh/w    step for sampling the sliding windows (e.g. every 4th pixel)
%
% Note:
% - bordering is automatically handled by padding the image
% - image is rescaled and cropped, leading to exact same size features
% - image can be any preprocessed data
% - possible todo: rotate, flip, aspect stretch the image, then extract and revert
%
% Authors: Hayko Riemenschneider (hayko@vision.ee.ethz.ch)

  IH = window_size(1);
  IW = window_size(2);
  num_feats = 0;


  num_scales = length(scales);
  for s = 1:num_scales
    I = imresize(II,1/scales(s));
    p = floor(window_size/2);
    I = padarray(I, p+1, 'symmetric', 'both');

    %generate a bunch of locations
    [h, w, nch] = size(I);
    offsetw = max(p(2),floor(mod(w,stridew)/2))+1;
    offseth = max(p(1),floor(mod(h,strideh)/2))+1;
    [loch,locw] = meshgrid(offseth:strideh:size(I,1)-IH-p(1)+1,...
                           offsetw:stridew:size(I,2)-IW-p(2)+1);

    % actual feature cropping
    switch(method)
        case {'ssd','chamfer','labelentropy'}
            level_feats=compute_raw_features(I,IW,IH,locw',loch');
        case 'hog'
            nori = 9;
            R = compute_gradient(I,nori);
            level_feats = compute_gradient_features(R,IW,IH,locw,loch,gw,gh);
        case 'cnn'
            level_feats=compute_cnn_features(I,IW,IH,locw',loch');
        case 'bow'
            level_feats=compute_dsift_features(I,IW,IH,locw',loch');
            
    end
    
    
    %correct for padding
    locw(:) = locw(:) - p(2);
    loch(:) = loch(:) - p(1);

    %concatenate the features
    count = size(level_feats,1);
    feats(num_feats+1:num_feats+count,:)  = level_feats;

    win_posw(num_feats+1:num_feats+count) = round(locw(:)*scales(s)); 
    win_posh(num_feats+1:num_feats+count) = round(loch(:)*scales(s)); 
    winw(num_feats+1:num_feats+count) = round(IW*scales(s)); 
    winh(num_feats+1:num_feats+count) = round(IH*scales(s)); 
    fprintf(1,'\tscale=%.3f [%dx%d], feats=%d\n',scales(s),round(IW* ...
                                                      scales(s)),round(IH*scales(s)),count);
    num_feats = num_feats + count;
  end; 
 
end
