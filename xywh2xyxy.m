function xyxy = xywh2xyxy(xywh)

xyxy = xywh;
xyxy(:,1:2) = xywh(:,1:2);
xyxy(:,3:4) = xywh(:,3:4)+xyxy(:,1:2)-1;
