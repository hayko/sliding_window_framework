function im = drawboxes(im, boxes, thick, c)
% im = drawboxes(im, boxes, thick, c)
% bbox format: x1, y1, x2, y2, score, xc, yc, w, h
% hayko extension

if (~exist('c','var')); c = [0 1 0]; end
if (~exist('thick','var')); thick = 5; end

% drawboxes(im, boxes)
% Draw boxes into image.

if(max(im(:)) <=1); factor = 1; else factor=255; end;
    
if(isempty(im))
    display('No image')
end

if ~isempty(boxes)
    
    if(size(c,1)==1)
        c = repmat(c,size(boxes, 1),1);
    end
    
    for i = 1:size(boxes, 1)
        x1 = min(size(im,1)-thick,max(thick+1,round(boxes(i,2))));
        y1 = min(size(im,2)-thick,max(thick+1,round(boxes(i,1))));
        x2 = min(size(im,1)-thick,max(thick+1,round(boxes(i,4))));
        y2 = min(size(im,2)-thick,max(thick+1,round(boxes(i,3))));
        
        %line([x1 x1 x2 x2 x1]', [y1 y2 y2 y1 y1]', 'color', c, 'linewidth', 3);
        im(x1:x2,y1-thick:y1+thick,1) = c(i,1)*factor;
        im(x1:x2,y1-thick:y1+thick,2) = c(i,2)*factor;
        im(x1:x2,y1-thick:y1+thick,3) = c(i,3)*factor;
        im(x1:x2,y2-thick:y2+thick,1) = c(i,1)*factor;
        im(x1:x2,y2-thick:y2+thick,2) = c(i,2)*factor;
        im(x1:x2,y2-thick:y2+thick,3) = c(i,3)*factor;
        im(x1-thick:x1+thick,y1:y2,1) = c(i,1)*factor;
        im(x1-thick:x1+thick,y1:y2,2) = c(i,2)*factor;
        im(x1-thick:x1+thick,y1:y2,3) = c(i,3)*factor;
        im(x2-thick:x2+thick,y1:y2,1) = c(i,1)*factor;
        im(x2-thick:x2+thick,y1:y2,2) = c(i,2)*factor;
        im(x2-thick:x2+thick,y1:y2,3) = c(i,3)*factor;
    end
end

