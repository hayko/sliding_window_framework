### sliding window detection


a simple demo for a sliding window detector.

generate set of window hypothesis locations.
create raw features densely for entire image.
collect and summarize score per window.
show score map and top 5 window locations.

implemented features: ssd, chamfer dist, bow
