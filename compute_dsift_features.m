function feats=compute_dsift_features(R,W,H,locW,locH)

load('data/bow_pos_neg', 'model_pos', 'model_neg', 'labeledFeatureResponsePos', 'labeledFeatureResponseNeg');

% labeledFeatureResponsePos stehn die projezierten pos daten = BOW_S+(X)
% labeledFeatureResponseNeg die projezierten neg daten= BOW_S-(X)
% model_pos is das pos bow - model_pos = BOW_S+
% model_neg das negative bow model_neg = BOW_S-


feats = cell(size(locW,1)*size(locW,2),1);

feat_indx = 1;
%loop over locations
for posi=1:size(locW,1)
    %display ([num2str([posi])])
    for posj=1:size(locW,2)
        posw = locW(posi,posj);
        posh = locH(posi,posj);
        
        % display ([num2str([posh posh+H-1 posw posw+W-1])])
        im_crop = im2double(R(posh:posh+H-1,posw:posw+W-1,:));
       feats_single_pos = getImageDescriptor(model_pos, single(im_crop));
        feats_single_pos = vl_homkermap((feats_single_pos), 1, 'kchi2', 'gamma', .5) ;

        feats_single_neg = getImageDescriptor(model_neg, single(im_crop));
        feats_single_neg = vl_homkermap(double(feats_single_neg), 1, 'kchi2', 'gamma', .5) ;

        posDist = min(pdist2(labeledFeatureResponsePos', feats_single_pos'));
        negDist = min(pdist2(labeledFeatureResponseNeg', feats_single_neg'));

        feats{feat_indx} = [posDist negDist];
        feat_indx = feat_indx+1;
    end
end
end


%feats_single_pos = getImageDescriptor(model_pos, single(resizedCropIm));
%feats_single_pos = vl_homkermap(feats_single_pos, 1, 'kchi2', 'gamma', .5) ;

% feats_single_neg = getImageDescriptor(model_neg, single(resizedCropIm));
% feats_single_neg = vl_homkermap(feats_single_neg, 1, 'kchi2', 'gamma', .5) ;
% 
% 
% posDist = min(pdist2(labeledFeatureResponsePos, feats_single_pos));
% negDist = min(pdist2(labeledFeatureResponseNeg, feats_single_neg));
% 
% score = negDist - posDist;
