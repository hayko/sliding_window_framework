% sliding window detector
clc;
clear;
close all;


% input: image, classifier function, parameter
% output, per evaluation point, score

im = imread('cameraman.tif');
im_template = single(imread('template.png'));
im_template_edges = edge(im_template, 'canny');




%feature parameters
stridew=4;
strideh=4;
window_size = [128 64];
window_size = size(im_template);
%scaleratio = 2;
scaleratio = 2^(1/4);
num_scales = 4;
scales = scaleratio.^(-num_scales:num_scales);
scales = 1;

%method='cnn';
%method='bow';
%method='chamfer';
method='ssd';
method='labelentropy';

switch(method)
    
    case 'chamfer'
        % chamfer matching - run champfer before hand, give as II to compute_windows_location
        warning ('needs vlfeat library')
    case 'ssd'
        % simple SSD template matching
        
    case 'bow'
        % bag of words with sift - extra sifts before hand, find those within bbox, histo
        
    case 'cnn'
        % neural network feature pooling
        
    case 'labelentropy'
        % score window by label entropy
        
        
        
end



file_list = dir('c*.png');

for file_idx = 2%1:length(file_list)
    
    im = imread(file_list(file_idx).name);
    tic
    
    switch(method)
        case 'chamfer'
            imSize = [size(im,1) size(im,2)] ;
            edges = zeros(imSize) + inf;
            edges(edge((im), 'canny')) = 0 ;
            [distanceTransform, neighbors] = vl_imdisttf(single(edges)) ;
            
            input = sqrt(distanceTransform);
            chamfer = @(x) sum(x(im_template_edges));
            func = chamfer;
            
            im_inline = im_template_edges*255;
            
        case 'ssd'
            ssd = @(x) sqrt(sum(sum((single(x)-im_template).^2)));
            func = ssd;
            input = im;
            im_inline = im_template;
            
            
        case 'labelentropy'
            
            eps = 1e-05;
            base = 10; % number of labels
            h = @(p, base, eps) 1 -sum(p.*log2(p+eps)/log2(base),2);
            h = @(p) 1 -sum(p.*log2(p+eps)/log2(base),2);
            
            % entropy test examples
            %c= [1 0 0 0 0 0 0 0 0 0];
            %h(c, base, eps)
            %h(c)
            
            %im=round(rand(5)*10);
            %c=hist(im(:),[1:10]);
            %h(c/sum(c), base, eps)
            %h(c/sum(c))
            
            
            im = imread('muensterhof_I2.png');% label image
            
            addpath('/scratch_net/biwisrv01_second/varcity/code/varcity/semseg/dataset')
            cm = colormap_zurich2rgb;
            base = length(cm);
            
            
            im_inline = zeros(2,2); % dummy
            
            func = @labelentropy; % entropy of over labels
            input = single(rgb2labelmap(im, cm));
            
            window_size = [100 100];
            window_size = [150 150];
            %window_size = [50 50];
            window_size = [50 50];
            im = imread('muensterhof_I2.png');% label image
            
    end
    
    % compute_windows_location extracts bboxes, but could also directly access image features
    tic
    [feats,win_posw,win_posh,winw,winh] = compute_windows_location(input,window_size, scales,strideh,stridew, method);
    toc

    tic
    [feats_rgb] = compute_windows_location(im,window_size, scales,strideh,stridew, method);
    toc
    
    feat = (cat(numel(size(feats{1}))+1,feats{:}));
    feats_max = max(feat);
    feats_min = min(feat);

    feat_rgb = (cat(numel(size(feats_rgb{1}))+1,feats_rgb{:}));
    
    % here: extract features per window again
    score = cellfun(func, feats, 'UniformOutput',true);
    toc
    
    switch(method)
        case 'labelentropy'
            % find the to label per window
            labelmap = cellfun(@labelmajority, feats, 'UniformOutput',true);
            score = [score labelmap];
        otherwise
            score = exp(-score / prod(window_size));
    end
    %  max(score), min(score)
    
    
    
    % collect bboxes
    bbox_score = xywh2xyxy([win_posw',win_posh',winw',winh' score]);
    
    % nms suppress results
    bbox_score_nms = bbox_nms(bbox_score,0.2);
    
    % select and display best bbox
    [val, pos] = max(bbox_score(:,5));
    bbox_best = bbox_score(pos,:);
    xyxy2cxcywh(bbox_best)
    
    clear c; c(:,3)=bbox_score_nms(:,5)+(1-bbox_score_nms(1,5));
    
    % draw best 10 bboxes
    top_n = 100;
    im_bbox = drawboxes(im, bbox_score_nms(1:top_n,:), 1, c);
    %im_bbox = drawboxes(im, bbox_best ,1);
    %im_bbox(1:size(im_template,1),1:size(im_template,2),1) = im_template;
    im_bbox(1:size(im_inline,1),1:size(im_inline,2),1) = im_inline;
    %im_crop = ICG_Image_CropByBbox(im, bbox_best);
    %im_bbox(1:size(im_template,1),1:size(im_template,2),1) = im_crop;
    
    figure, imshow(im_bbox)
    
    % vis confidence map
    if(1)
        bbox_score_cxcy = xyxy2cxcywh(bbox_score);
        pts = sub2ind([size(im,1) size(im,2)],bbox_score_cxcy(:,2),bbox_score_cxcy(:,1));
        
        conf = zeros(size(im,1),size(im,2));
        conf(pts) = score;
        
        conf2 = conf(:,window_size(2)/2+1:strideh:size(conf,2)-(window_size(2)/2+1)+2);
        conf2 = conf2(window_size(1)/2+1:strideh:size(conf,1)-(window_size(1)/2+1)+2,:);
        figure, mesh(conf2)
        
        
        [x,y] = meshgrid(window_size(2)/2+1:strideh:size(conf,2)-(window_size(2)/2+1)+2,  window_size(1)/2+1:strideh:size(conf,1)-(window_size(1)/2+1)+2);
        [xi,yi] = meshgrid(1:size(im,2),1:size(im,1));
        zi = interp2(x,y,conf2,xi,yi);
        figure, imshow(zi,[])
        
    end
    
    %% show crops
    im_rgb =  imread('muensterhof_I2rgb.png');% label image
    im_crop = uint8(255*ones(size(im_rgb)));
    im_label_crop = uint8(255*ones(size(im_rgb)));
    
    for idx = 1:size(bbox_score_nms,1)
      
        if ((bbox_score_nms(idx,5)>0.95))
            class_idx = 1; % windows
            class_idx = 2; % wall
            %class_idx = 3; % balcony
            %class_idx = 7; % shop
            
        if((bbox_score_nms(idx,6)==class_idx)) 
        im_crop(bbox_score_nms(idx,2):bbox_score_nms(idx,4),bbox_score_nms(idx,1):bbox_score_nms(idx,3),:)=im_rgb(bbox_score_nms(idx,2):bbox_score_nms(idx,4),bbox_score_nms(idx,1):bbox_score_nms(idx,3),:);
        im_label_crop(bbox_score_nms(idx,2):bbox_score_nms(idx,4),bbox_score_nms(idx,1):bbox_score_nms(idx,3),:)=im(bbox_score_nms(idx,2):bbox_score_nms(idx,4),bbox_score_nms(idx,1):bbox_score_nms(idx,3),:);
        
        
        end
        end
    end
    figure,
    subplot(1,4,1), imshow(im_rgb)
    subplot(1,4,2), imshow(im)
    subplot(1,4,3), imshow(im_crop)
    subplot(1,4,4), imshow(im_label_crop)
    
    save('muensterhof_I2.mat','feat_rgb','bbox_score','bbox_score_nms');
    
end % filelist
