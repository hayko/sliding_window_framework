function xywh = xyxy2cxcywh(xyxy)

xywh=xyxy;
xywh(:,1:2) = round((xyxy(:,1:2)+xyxy(:,3:4))/2);
xywh(:,3:4) = xyxy(:,3:4)-xyxy(:,1:2)+1;
